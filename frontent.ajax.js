(function ($) {
    $('.product-vote-button-up').click (function () {
        var productID = $(this).attr ('productID') ;

        $.ajax ({
            url:      '/wp-admin/admin-ajax.php',
            type:     'POST',
            dataType: 'JSON',
            data: {
                // the value of data.action is the part AFTER 'wp_ajax_' in
                // the add_action ('wp_ajax_xxx', 'yyy') in the PHP above
                action: 'call_your_function',
                // ANY other properties of data are passed to your_function()
                // in the PHP global $_REQUEST (or $_POST in this case)
                id : productID
                },
            success: function (resp) {
                if (resp.success) {
                    // if you wanted to use the return value you set 
                    // in your_function(), you would do so via
                    // resp.data, but in this case I guess you don't
                    // need it
                    $('#product-' + productID + ' .item-product-footer-vote-container').html ('Thanks for your vote!') ;
                    }
                else {
                    // this "error" case means the ajax call, itself, succeeded, but the function
                    // called returned an error condition
                    alert ('Error: ' + resp.data) ;
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                // this error case means that the ajax call, itself, failed, e.g., a syntax error
                // in your_function()
                alert ('Request failed: ' + thrownError.message) ;
                },
            }) ;
        }) ;
    })(jQuery) ;