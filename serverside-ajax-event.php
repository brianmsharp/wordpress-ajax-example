// hook into admin-ajax
// the text after 'wp_ajax_' and 'wp_ajax_no_priv_' in the add_action() calls
// that follow is what you will use as the value of data.action in the ajax
// call in your JS

// if the ajax call will be made from JS executed when user is logged into WP,
// then use this version
add_action ('wp_ajax_call_your_function', 'your_function') ;
// if the ajax call will be made from JS executed when no user is logged into WP,
// then use this version
add_action ('wp_ajax_nopriv_call_your_function', 'your_function') ;

function
your_function ()
{
    if (!isset ($_REQUEST['id'])) {
        // set the return value you want on error
        // return value can be ANY data type (e.g., array())
        $return_value = 'your error message' ;

        wp_send_json_error ($return_value) ;
        }

    $id = intval ($_REQUEST['id']) ;
    // do processing you want based on $id

    // set the return value you want on success
    // return value can be ANY data type (e.g., array())
    $return_value = 'your success message/data' ;

    wp_send_json_success ($return_value) ;
}